﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PracticaExceptions.Exceptions;

namespace PracticaExceptions.Test
{
    [TestClass]
    public class ApplicationExceptionTest
    {
        [TestMethod]
        public void TApplicationECApplicationE()
        {
            //throw: applicationException
            //catch: applicationException
            //finally: yes
            //end: no
            try
            {

            }
            catch (AlumnoException)
            {
                Assert.Fail("alumno");
            }
            catch (PersonaException)
            {
                Assert.Fail("persona");
            }
            catch(ApplicationException)
            {
                Assert.IsTrue(true);
            }
            catch(Exception)
            {
                Assert.Fail("exception");
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TApplicationCException()
        {
            //Throw: applicationException
            //Expected catch : Exception
            //finally: no
            //end: yes
            try
            {
                throw new ApplicationException();
            }
            catch(ProfesorException)
            {
                Assert.Fail("profesor");
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void TApplicationCNada()
        {
            //throw: applicationException
            //Expected catch: no -> expected exception
            //finally: yes
            //end: yes
            try
            {
                throw new ApplicationException();
            }
            catch (AsistenteException)
            {
                Assert.Fail("asistente");
            }
            finally
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
    }
}
