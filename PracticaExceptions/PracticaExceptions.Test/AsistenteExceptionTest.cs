﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PracticaExceptions.Exceptions;

namespace PracticaExceptions.Test
{
    [TestClass]
    public class AsistenteExceptionTest
    {
        [TestMethod]
        public void TAsistenteECAsistenteE()
        {
            //throw: assistenteExcpetion
            //expected catch: AsistenteException
            //finally: no
            //end: no
            try
            {
                throw new AsistenteException();
            }
            catch (AlumnoException)
            {
                Assert.Fail("alumno");
            }
            catch(AsistenteException)
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TAsistenteECPersonaE()
        {
            //throw: AsistenteException
            //catch: PersonaException
            //finally: yes
            //end: no
            try
            {
                throw new AsistenteException();
            }
            catch (ProfesorException)
            {
                Assert.Fail("profesor");
            }
            catch (PersonaException)
            {
                Assert.IsTrue(true);
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TAsistenteECApplicationE()
        {
            //throw: AsistenteException
            //expected catch: ApplicationException
            //finally: no
            //end: yes
            try
            {
                throw new AsistenteException();
            }
            catch (ApplicationException)
            {
                Assert.IsTrue(true);
            }
            catch(Exception)
            {
                Assert.Fail("aexception");
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void TAsistenteECException()
        {
            //Throw: AsistenteException
            //Catch: Exception
            //finally: yes
            //end: yes
            try
            {
                throw new AsistenteException();
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            finally
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(AsistenteException))]
        public void TAsistenteECNada()
        {
            //throw: AsistenteException
            //expected catch: nothing -> expected exception
            //finally: no
            //end: no
            throw new AsistenteException();
        }
    }
}
