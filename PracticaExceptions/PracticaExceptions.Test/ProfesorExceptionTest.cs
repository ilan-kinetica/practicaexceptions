﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PracticaExceptions.Exceptions;

namespace PracticaExceptions.Test
{
    [TestClass]
    public class ProfesorExceptionTest
    {
        [TestMethod]
        public void TProfesorECProfesorE()
        {
            //thorw: ProfesorException
            //expected catch: Profesor exception
            //finally: no
            //end: yes
            try
            {
                throw new ProfesorException();
            }
            catch (AlumnoException)
            {
                Assert.Fail("alumno");
            }
            catch (AsistenteException)
            {
                Assert.Fail("asistente");
            }
            catch (ProfesorException)
            {
                Assert.IsTrue(true);
            }
            catch (ApplicationException)
            {
                Assert.Fail("application");
            }
            catch (Exception)
            {
                Assert.Fail("exception");
            }
            finally
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void TProfesorECPersonaE()
        {
            //trhow: ProfesorException
            //Expected catch: PersonaException
            //finally: no
            //end: yes
            try
            {
                throw new ProfesorException();
            }
            catch (AlumnoException)
            {
                Assert.Fail("alumno");
            }
            catch(PersonaException)
            {
                Assert.IsTrue(true);
            }
            catch(ApplicationException)
            {
                Assert.Fail("application");
            }
            catch(Exception)
            {
                Assert.Fail("exception");
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void TProfesorECApplicationE()
        {
            //throw:ProfesorException
            //expected catch: ApplicationException
            //finally: yes
            //end: no
            try
            {
                throw new ProfesorException();
            }
            catch (AsistenteException)
            {
                Assert.Fail("asistente");
            }
            catch(ApplicationException)
            {
                Assert.IsTrue(true);
            }
            catch(Exception)
            {
                Assert.Fail("exception");
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TProfesorECException()
        {
            //throw: profesorException
            //catch: exception
            //finally: no
            //end: no
            try
            {
                throw new ProfesorException();
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        [ExpectedException(typeof(ProfesorException))]
        public void TProfesorECNada()
        {
            //throw: ProfesorException
            //expected catch: nothing -> expected exception
            //finally: no
            //end: no
            throw new ProfesorException();
        }
    }
}
