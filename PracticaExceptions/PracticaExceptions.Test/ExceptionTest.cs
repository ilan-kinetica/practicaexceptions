﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PracticaExceptions.Exceptions;

namespace PracticaExceptions.Test
{
    [TestClass]
    public class ExceptionTest
    {
        [TestMethod]
        public void TExceptionCException()
        {
            //throw: exception
            //catch: exception
            //finally: yes
            //end: yes
            try
            {
                throw new Exception();
            }
            catch(AlumnoException)
            {
                Assert.Fail("alumno");
            }
            catch(AsistenteException)
            {
                Assert.Fail("asistente");
            }
            catch(ProfesorException)
            {
                Assert.Fail("profesor");
            }
            catch(PersonaException)
            {
                Assert.Fail("persona");
            }
            catch(ApplicationException)
            {
                Assert.Fail("application");
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            finally
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MyTestMethod()
        {
            //throw: exception
            //catch: nothing -> expected exception
            //finally: yes
            //end: no
            try
            {
                throw new Exception();
            }
            catch (ApplicationException)
            {
                Assert.Fail("application");
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
    }
}
