﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PracticaExceptions.Exceptions;

namespace PracticaExceptions.Test
{
    [TestClass]
    public class PersonaExceptionTest
    {
        [TestMethod]
        public void TPersonaECPersonaE() //ThrowPersonaExceptionCatchPErsonaException
        {
            //throw: PersonaException
            //Expected catch: PersonaException
            //finally: yes
            //end: yes
            try
            {
                throw new PersonaException("PersonaE");
            }
            catch (AlumnoException) //inherits from PersonaException
            {
                Assert.Fail("alumnoE");
            }
            catch (PersonaException)
            {
                Assert.IsTrue(true);
            }
            catch (ApplicationException)
            {
                Assert.Fail("applicationE");
            }
            catch (Exception)
            {
                Assert.Fail("exception");
            }
            finally
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        public void TPersonaECApplicationE()
        {
            //throw: PersonaException
            //Expected catch: ApplicationException
            //finally: yes
            //end: no
            try
            {
                throw new PersonaException("PersonaE");
            }
            catch (AsistenteException) //inherits from PersonaException
            {
                Assert.Fail("alumnoE");
            }
            catch (ApplicationException)
            {
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.Fail("exception");
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TPersonaECException()
        {
            //throw: PersonaException
            //Expected catch: Exception
            //finally: yes
            //end: yes
            try
            {
                throw new PersonaException("PersonaE");
            }
            catch (ProfesorException) //inherits from PersonaException
            {
                Assert.Fail("alumnoE");
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            finally
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(PersonaException))]
        public void TPersonaECNada()
        {
            //throw: PersonaException
            //expected catch: nothing -> expected exception
            //finally: no
            //end: no
            try
            {
                throw new PersonaException();
            }
            catch(AlumnoException)
            {
                Assert.Fail("alumno");
            }
        }
    }
}
