﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PracticaExceptions.Exceptions;

namespace PracticaExceptions.Test
{
    [TestClass]
    public class AlumnoExceptionTest
    {
        [TestMethod]
        public void TAlumnoECAlumnoE()
        {
            //throw: AlumnoException
            //Expected catch: AlumnoException
            //finally: no
            //end: yes
            try
            {
                throw new AlumnoException();
            }
            catch (AsistenteException)
            {
                Assert.Fail("asistente");
            }
            catch (ProfesorException)
            {
                Assert.Fail("profesor");
            }
            catch (AlumnoException)
            {
                Assert.IsTrue(true, "alumno");
            }
            catch (PersonaException)
            {
                Assert.Fail("persona");
            }
            catch (ApplicationException)
            {
                Assert.Fail("application");
            }
            catch (Exception)
            {
                Assert.Fail("exception");
            }
            Assert.IsTrue(true, "end");
        }
        [TestMethod]
        public void TAlumnoECPersonaE()
        {
            //throw: AlumnoException
            //Expected catch: PersonaException
            //finally: yes
            //end: no
            try
            {
                throw new AlumnoException();
            }
            catch (PersonaException)
            {
                Assert.IsTrue(true);
            }
            catch (ApplicationException)
            {
                Assert.Fail("application");
            }
            catch (Exception)
            {
                Assert.Fail("exception");
            }
            finally
            {
                Assert.IsTrue(true);
            }
        }
        [TestMethod]
        public void TAlumnoECApplicationE()
        {
            //throw: AlumnoException
            //Expected catch: ApplicationException
            //finally: no
            //end: no
            try
            {
                throw new AlumnoException();
            }
            catch (ProfesorException)
            {
                Assert.Fail("profesor");
            }
            catch (ApplicationException)
            {
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.Fail("exception");
            }
        }
        [TestMethod]
        public void TAlumnoECException()
        {
            //throw: alumnoexception
            //expected catch: Axception
            //finally: yes
            //end: yes
            try
            {
                throw new AlumnoException();
            }
            catch (Exception)
            {
                Assert.IsTrue(true);
            }
            finally
            {
                Assert.IsTrue(true);
            }
            Assert.IsTrue(true);
        }
        [TestMethod]
        [ExpectedException(typeof(AlumnoException))]
        public void TAlumnoECNada()
        {
            //throw: alumno exception
            //expected catch: nothing -> expected exception
            //finally: no
            //end: no
            throw new AlumnoException();
        }
    }
}
