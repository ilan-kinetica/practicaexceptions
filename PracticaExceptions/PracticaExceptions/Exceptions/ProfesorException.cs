﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaExceptions.Exceptions
{
    public class ProfesorException : PersonaException
    {
        public ProfesorException() { }
        public ProfesorException(string message) : base(message) { }
    }
}
