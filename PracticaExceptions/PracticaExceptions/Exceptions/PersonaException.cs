﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaExceptions.Exceptions
{
    public class PersonaException : ApplicationException
    {
        public PersonaException() { }
        public PersonaException(string message) : base(message) { }
    }
}
