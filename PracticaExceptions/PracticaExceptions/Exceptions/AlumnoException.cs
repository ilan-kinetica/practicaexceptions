﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaExceptions.Exceptions
{
    public class AlumnoException : PersonaException
    {
        public AlumnoException() { }
        public AlumnoException(string message) : base(message) { }
    }
}
