﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaExceptions.Exceptions
{
    public class AsistenteException : PersonaException
    {
        public AsistenteException() { }
        public AsistenteException(string message) : base(message) { }
    }
}
